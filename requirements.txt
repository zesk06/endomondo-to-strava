path
click
python-dotenv
selenium
virtualenvwrapper
logzero
requests
black
isort
neovim
flask
###########
# we must pin jedi/parso due to a bug
# see bug https://github.com/ipython/ipython/issues/12745
ipdb
ipython
jedi==0.17.2
parso==0.7.1
############
