# endomondo to strava

Create a venv, and install dependencies in it.
```bash
pip3 install -r requirements
```

## Create an API Application

In your strava account, create a new API Application
Get your __client id__ and __client secret__.

Create a .env file with that content:
```ini
CLIENT_ID=YOUR_CLIENT_ID
CLIENT_SECRET=YOUR_CLIENT_SECRET
```

## launch OAuth server

Then, launch the Oauth server ``python3 0_server.py``
- Open the [index](http://localhost:5000)
- click on the __auth_page__
- click on the link to request a bearer token

The token is valid for a couple of hour.
Then you shall reauth/regen token

## import your workouts

- Get your endomondo archive
- unzip it, and locate the Workouts folder (It contains many *.tcx files)
- launch ``python3 upload_tcx.py --limit 100 FOLDER_WITH_TCX``
- TCX files are uploaded, a file nammed ``original_filename.tcx.id`` is created to store request id
  and prevent from sending the same acitivity again
- when request limit is reached, a 429 error code is returned by strava, wait 30min and relaunch.
