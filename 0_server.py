#!/usr/bin/env python
# encoding: utf-8

from dotenv import load_dotenv
from flask import Flask,request,render_template,redirect,url_for
from path import Path
import json
import os
import requests

app = Flask(__name__)

load_dotenv()
client_id = os.getenv("CLIENT_ID")
client_secret = os.getenv("CLIENT_SECRET")
 
@app.route("/")
def index():
    url="http://www.strava.com/oauth/authorize?"
    url += f"client_id={client_id}"
    url+="&response_type=code"
    url+="&redirect_uri=http://localhost:5000/set_access_code"
    url+="&approval_prompt=force&scope=profile:read_all,activity:write,activity:read"
    # we may be redirected from set_access_code
    access_code = None
    if "access_code" in request.args:
        access_code = request.args.get("access_code")
    data = None
    if "data" in request.args:
        data = request.args.get("data")
    return render_template("index.html", url=url, access_code=access_code, data=data)


@app.route("/set_access_code")
def set_access_code():
    """called from strava auth page callback"""
    code = request.args.get("code")    
    with open("access_code", "w") as ac_f:
        ac_f.write(code)
    return redirect(url_for("index",access_code=code))

@app.route("/get_token")
def get_token():
    with open("access_code", "r") as ac_f:
        access_code = ac_f.readline()
    print(f"access code is {access_code}")
    # Make Strava auth API call with your
    # client_code, client_secret and code
    response = requests.post(
        url="https://www.strava.com/oauth/token",
        data={
            "client_id": client_id,
            "client_secret": client_secret,
            "code": access_code,
            "grant_type": "authorization_code",
        },
    )
    # Save json response as a variable
    strava_tokens = response.json()
    # Save tokens to file
    with open("strava_tokens.json", "w") as outfile:
        json.dump(strava_tokens, outfile)
    # to check it's worked properly
    data = None
    with open("strava_tokens.json") as check:
        data = json.load(check)
    print(data)
    return redirect(url_for("index",data=data))

if __name__ == '__main__':
    # Bind to PORT if defined, otherwise default to 5000.
    app.run(host='0.0.0.0', port=5000, debug=True)
