#!/usr/bin/env python
# encoding: utf-8

"""Un module pour ouvrir et exploiter un TCX garmin"""

import sys
from xml.etree import ElementTree as ET

NS={"garmin":"http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2"}
class TCX:
    def __init__(self, file_p):
        self.file_p = file_p
        self.tree = ET.parse(self.file_p)

    @property
    def activity(self):
        activity_el =  self.tree.find(".//garmin:Activity", NS)
        activity =  activity_el.attrib["Sport"]
        return activity

    @property
    def intensity(self):
        intensity_el =  self.tree.find(".//garmin:Intensity", NS)
        intensity = intensity_el.text 
        return intensity
    
    def __repr__(self):
        return f"TCX(file_p=\"{self.file_p}\", activity=\"{self.activity}\", intensity=\"{self.intensity}\")"

def main():
    """print TCX info from given TCX file"""
    mtcx = TCX(sys.argv[1])   
    print(mtcx)

if __name__ == "__main__":
    main()
