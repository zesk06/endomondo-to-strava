#!/usr/bin/env python
# encoding: utf-8

from dotenv import load_dotenv
from path import Path
import json
import os
import sys
from logzero import logger
import logging
import requests
import click
import tcx
import time

logger.setLevel(logging.DEBUG)

def get_bearer_token():
    with open("strava_tokens.json") as check:
        data = json.load(check)
        return data["access_token"]
    return None

CODE_EMPTY = 0
CODE_RETRY = -1

@click.command()
@click.argument("tcx_dir")
@click.option("--limit", "-l", default=-1)
def upload_dir(tcx_dir, limit=-1):
    tcx_dir = Path(tcx_dir)
    uploaded = 0
    for tcx_path in sorted(tcx_dir.glob("*.tcx")):
        new_id = CODE_RETRY
        while new_id == CODE_RETRY:
            logger.info(f" send {tcx_path}")
            new_id = upload(tcx_path)
        if new_id != CODE_EMPTY:
            uploaded += 1
        if uploaded == limit:
            break
    

def upload(tcx_path: Path):
    id_path = Path(f"{tcx_path}.id")
    if id_path.exists():
        req_id = id_path.read_text()
        logger.debug(f"SKIP {tcx_path}: already exist: {req_id}")
        return None
    bearer_token = get_bearer_token()
    logger.debug(f"{bearer_token=}")
    mtcx = tcx.TCX(tcx_path)
    logger.debug(f"Upload [{mtcx.activity}] {tcx_path}")
    activity_type = 'workout'
    if mtcx.activity == "Running":
        activity_type = "run"
    elif mtcx.activity == "Biking":
        activity_type = "ride"
    else:
        return None

    headers = {"Authorization": f"Bearer {bearer_token}"}
    data={
        "activity_type": activity_type,
        "data_type": "tcx",
        # "name": "pyupload",
    }
    files= {"file": tcx_path.open("rb")}
    response = requests.post(
        url="https://www.strava.com/api/v3/uploads",
        headers=headers,
        data=data,
        files=files,
    )
    resp_json = response.json()
    if response.status_code == 201:
        resp_id = resp_json["id_str"]
        logger.info(f"OK: id {resp_id} : {tcx_path}")
        id_path.write_text(resp_id)
        return resp_id
    elif response.status_code == 400 and "The file is empty" in resp_json["error"]:
        logger.error(f"Failed to upload: {tcx_path}")
        logger.error(f"{response}")
        logger.error("Lap GPS points list is empty")
        logger.error(f"-- write  empty to {id_path} to skip file at next call")
        resp_id = CODE_EMPTY
        id_path.write_text(str(resp_id))
        return resp_id
    elif response.status_code == 401:
        logger.error(f"Failed to upload: {tcx_path}")
        logger.error(f"{response}")
        logger.error("token may be outdated")
        sys.exit(1)
    elif response.status_code == 429:
        logger.error(f"Failed to upload: {tcx_path}")
        logger.error(f"{response}")
        logger.error("rate limit exceed, retry in 20 min")
        for i in range(4):
            print(f"{i}/3: wait 5min")
            time.sleep(300)
        return CODE_RETRY
    # an error occured
    logger.error(f"Failed to upload: {tcx_path}")
    logger.error(f"{response}")
    logger.error(f"{response.content}")
    return None

if __name__ == "__main__":
    upload_dir() # pylint: disable=no-value-for-parameter
